// console.log('hello')

console.log(document.querySelector("#txt-first-name"))

console.log(document)
// document refers to the whole page
// querySelector is used to select a specific element
console.log(document.getElementById("span-full-name"))
/*
	Alternative:

	document.getElementById("txt-first-name")
	document.getElementsByClassName()
	document.getElementsByTagName()
*/

const txtFirstName=document.querySelector("#txt-first-name")
const spanFullName= document.querySelector("#span-full-name")
const txtLastName=document.querySelector("#txt-last-name")
// Event Listener
/*
	selectedElement.addEventListener('event',function)
*/

function printFullName(event) {
	spanFullName.innerHTML = txtFirstName.value
	spanFullName.innerHTML += ' '+  txtLastName.value
}



txtFirstName.addEventListener('keyup',printFullName)

txtLastName.addEventListener('keyup',printFullName)